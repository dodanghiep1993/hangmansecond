import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.*;
import java.util.stream.Collectors;


public class HangmanSecond {
    static Path filepath = Paths.get(System.getProperty("user.dir") + "/src/secretwords.txt");
    static Path filepath1 = Paths.get(System.getProperty("user.dir") + "/src/art.csv");
    static Path filepath2 = Paths.get(System.getProperty("user.dir") + "/src/score.txt");
    public static void main(String[] args) {
        Scanner getInput = new Scanner(System.in);
        Random rand = new Random();
        String sword = "";
        int live = 6;
        StringBuilder st = new StringBuilder();

        try {
            List<String> secretwords = Files.readAllLines(filepath);
            sword = secretwords.get(rand.nextInt(secretwords.size()));
        } catch (Exception e) {
            System.out.println("File doesn't exist or filepath is wrong");
            System.exit(-1);
        }
        List<String> swor1 = new ArrayList<>(Arrays.asList(sword.split("")));
        List<String> swor = new ArrayList<>(Arrays.asList(sword.split("")));
        Object[] b = swor.stream().map(e -> e = "_").collect(Collectors.toList()).toArray();

        swor.stream().forEach(str -> System.out.print(str));
        System.out.println();
        System.out.println("Welcome to Hangman");
        System.out.println();
        System.out.print("What is your name: ");
        String name = "";
        try {
            if(getInput.hasNextInt()) {
                throw new Exception();
            } else {
                name = getInput.next();
                if(!(name.charAt(0) >= 'a' && name.charAt(0) <= 'z') && !(name.charAt(0) >= 'A' && name.charAt(0) <= 'Z')) {
                    throw new Exception();
                }
            }
        } catch (Exception e) {
            System.out.println();
            System.out.println("Invalid input! input should be words or letters");
            System.exit(-1);
        }
        System.out.println();

        while(true) {
            boolean found = false;
            drawHangman(live);

            System.out.println("Missed letters: " + st);
            System.out.println();


            Arrays.stream(b).forEach(a -> System.out.print(a));
            System.out.println();
            System.out.println();
            System.out.println("Guess a letter.");
            System.out.println();
            String letter = "";
            try {
                if(getInput.hasNextInt()) {
                    throw new Exception();
                } else {
                    letter = getInput.next();
                    if(letter.length() > 1 || !(letter.charAt(0) >= 'a' && letter.charAt(0) <= 'z')) {
                        throw new Exception();
                    }
                }
            } catch (Exception e) {
                System.out.println();
                System.out.println("Invalid input! input should be only one letter");
                break;
            }
            System.out.println();


            if ((Arrays.asList(b).contains(letter)) || (st.toString().contains(letter))) {
                System.out.println("You have already guessed that letter. Choose again.\n");
                found = true;
            }


            if (swor.contains(letter)) {
                mapp(swor1, b, letter);
                found = true;
            }

            if (!(Arrays.asList(b).contains(letter)) && !(new String(st).contains(letter))) {
                st.append(letter.charAt(0));
            }

            if(!found) {
                live--;
            }

            if((Arrays.asList(b).containsAll(swor))) {
                System.out.println("Yes! The secret word is \"" + sword + "\"! You have won!");
                try {
                    Files.write(filepath2, (name + " won 1\n").getBytes(), StandardOpenOption.CREATE, StandardOpenOption.APPEND);
                    List<String> wandl = Files.readAllLines(filepath2);
                    List<String> w = wandl.stream().filter(s -> s.contains("won")).collect(Collectors.toList());
                    List<String> l = wandl.stream().filter(s -> s.contains("lost")).collect(Collectors.toList());
                    if (w.size() > l.size()) {
                        System.out.println("\nYou won more than lost");
                        if (w.size() - l.size() >= 2) {
                            System.out.println("\nYou have a high score: " + (w.size() - l.size()));
                        }
                    } else if (w.size() < l.size()){
                        System.out.println("\nyou lost more than won: " + (l.size() - w.size()));
                    } else {
                        System.out.println("\nYour score is 0");
                    }
                } catch (Exception e) {
                    System.out.println("File doesn't exit");
                    System.exit(-1);
                }
                break;
            }

            if (live <= 0) {
                drawHangman(live);
                System.out.println("You are dead! The secret word is \"" + sword + "\"");
                try {
                    Files.write(filepath2, (name + " lost 1\n").getBytes(), StandardOpenOption.CREATE, StandardOpenOption.APPEND);
                    List<String> wandl = Files.readAllLines(filepath2);
                    List<String> l = wandl.stream().filter(s -> s.contains("lost")).collect(Collectors.toList());
                    List<String> w = wandl.stream().filter(s -> s.contains("won")).collect(Collectors.toList());
                    if (w.size() > l.size()) {
                        System.out.println("\nYou won more than lost");
                        if (w.size() - l.size() >= 2) {
                            System.out.println("\nYou have a high score: " + (w.size() - l.size()));
                        }
                    } else if (w.size() < l.size()) {
                        System.out.println("\nyou lost more than won: " + (l.size() - w.size()));
                    } else {
                        System.out.println("\nYour score is 0");
                    }
                } catch (Exception e) {
                    System.out.println("File doesn't exit");
                    System.exit(-1);
                }
                break;
            }
        }
    }

    public static void mapp(List<String> sword, Object[] answer, String input) {

        sword.stream().forEach(s -> {
            if (s.contains(input)) {
                answer[sword.indexOf(s)] = s;
                sword.set(sword.indexOf(s), "_");
            }
        });
    }

    public static void drawHangman(int l) {
        Object[] store;
        List<String> draw = new ArrayList<>();
        try {
            draw = Files.readAllLines(filepath1);
        } catch (Exception e) {
            System.out.println("File doesn't exit");
            System.exit(-1);
        }
        if(l == 6) {
            store = draw.get(0).split(",");
            Arrays.stream(store).sequential().forEach(System.out::println);
            System.out.println();
        }
        else if(l == 5) {
            store = draw.get(1).split(",");
            Arrays.stream(store).sequential().forEach(System.out::println);
            System.out.println();
        }
        else if(l == 4) {
            store = draw.get(2).split(",");
            Arrays.stream(store).sequential().forEach(System.out::println);
            System.out.println();
        }
        else if(l == 3) {
            store = draw.get(3).split(",");
            Arrays.stream(store).sequential().forEach(System.out::println);
            System.out.println();
        }
        else if(l == 2) {
            store = draw.get(4).split(",");
            Arrays.stream(store).sequential().forEach(System.out::println);
            System.out.println();
        }
        else if(l == 1) {
            store = draw.get(5).split(",");
            Arrays.stream(store).sequential().forEach(System.out::println);
            System.out.println();
        }
        else{
            store = draw.get(6).split(",");
            Arrays.stream(store).sequential().forEach(System.out::println);
            System.out.println();
        }
    }
}
